function mascara(obj, func, param) {
  v_obj = obj
  v_fun = func
  v_param = param
  setTimeout("execmascara()", 1)
}

function execmascara() {
  v_obj.value = v_fun(v_obj.value, v_param)
}

// Máscara para CPF
function cpf(value) {
  value = value.replace(/\D/g, "")
  value = value.replace(/(\d{3})(\d)/, "$1.$2")
  value = value.replace(/(\d{3})(\d)/, "$1.$2")
  value = value.replace(/(\d{3})(\d{1,2})$/, "$1-$2")

  return value
}

// Máscara para CNPJ
function cnpj(value) {
  value = value.replace(/\D/g, "")
  value = value.replace(/(\d{2})(\d)/, "$1.$2")
  value = value.replace(/(\d{3})(\d)/, "$1.$2")
  value = value.replace(/(\d{3})(\d)/, "$1/$2")
  value = value.replace(/(\d{4})(\d{1,2})$/, "$1-$2")

  return value
}

// Máscara para CEP
function cep(value) {
  value = value.replace(/\D/g, "")
  value = value.replace(/^(\d{5})(\d)/, "$1-$2")
  return value
}

// Máscara para SOMENTE NÚMEROS
function onlyNumber(value) {
  return value.replace(/\D/g, "")
}

// Máscara para DATA
function toDate(value) {
  value = value.replace(/\D/g, "");
  value = value.replace(/(\d{2})(\d)/, "$1/$2");
  value = value.replace(/(\d{2})(\d)/, "$1/$2");

  value = value.replace(/(\d{2})(\d{2})$/, "$1$2");
  return value;
}

// Máscara para TELEFONE
function phone(value) {
  value = value.replace(/\D/g, "")
  value = value.replace(/^(\d\d)(\d)/g, "($1) $2")
  value = value.replace(/(\d{5})(\d)/, "$1-$2")
  return value
}

// Máscara para DECIMAL
function decimal(value, fractionDigit) {
  let zeros = ""
  if (value.replace(",", "").length == 1) {
    for (let i = 0; i < fractionDigit - 1; i++) { zeros += "0"; }
    value = `0,${zeros}${value}`;
  }

  else {
    let number = value.replace(/\D/g, '');
    let zeros = "";
    let divider = "";

    for (let i = 0; i < fractionDigit; i++) { zeros += "0"; }
    divider = parseInt(`1${zeros}`);

    number = number / divider;
    value = number.toLocaleString('pt-br', { minimumFractionDigits: fractionDigit });
  }

  return value
}

// Máscara para MOEDA
function money(value, param) {
  document.getElementById(param[1]).innerHTML = param[0];

  if (value.replace(",", "").length == 1) { value = `0,0${value}` }

  else {
    value = (value.replace(/\D/g, '') / 100).toLocaleString('pt-br', { minimumFractionDigits: 2 });
  }

  return value
}

// Máscara para EMAIL
function email(value) {
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (!value.match(mailformat)) {
    validateForm();
  }

  return value
}

