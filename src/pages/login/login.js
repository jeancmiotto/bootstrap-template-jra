document.addEventListener("DOMContentLoaded", function () {
});

window.onresize = elementsOnResize;

document.getElementById("bt_login").onclick = function () {
  validateForm();
}

function validateForm() {
  if (document.getElementById('v_email').value == '' || document.getElementById('v_password').value == '') {
    document.getElementById('modal_validateForm_message').innerHTML = 'Para se autenticar, insira o usuário e senha.'
    showModal();
  }

  else if (document.getElementById('v_email').value != 'admin@admin.com' || document.getElementById('v_password').value != 'admin') {
    document.getElementById('modal_validateForm_message').innerHTML = 'Usuário ou senha incorretos. Por favor, verifique.'
    showModal();
  }

  else { window.location.replace("../view/view.html"); }
}

function showModal() {
  var modal_validateForm = new bootstrap.Modal(document.getElementById("modal_validateForm"), { keyboard: false });
  modal_validateForm.show();
}

function elementsOnResize() {
  var widthSize = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  var heightSize = window.innerHeight || document.documentElement.clientHeight | document.body.clientHeight;

  if (widthSize <= 768) {
    document.getElementById("divContent").style.display = "none";
  }

  else { document.getElementById("divContent").style.display = ""; }
}

