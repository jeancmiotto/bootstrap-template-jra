document.addEventListener("DOMContentLoaded", function () {
  listTable();

  var order = document.getElementById('order-collapse')
  var registration = document.getElementById('registration-collapse')
  var foundation = document.getElementById('foundation-collapse')
  var threeLevel = document.getElementById('threelevel-collapse')
  var threeLevel_1 = document.getElementById('threelevel1-collapse')
  var threeLevel_2 = document.getElementById('threelevel2-collapse')

  // Ações Menu ORDENS
  order.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-order').classList.replace('active', 'text-white');
  });

  order.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-order').classList.replace('text-white', 'active');

    if (threeLevel.classList.contains('show')) { toggleElement(threeLevel.id); }
    if (registration.classList.contains('show')) { toggleElement(registration.id); }
    if (foundation.classList.contains('show')) { toggleElement(foundation.id); }
  });

  // Ações Menu CADASTROS
  registration.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-registration').classList.replace('active', 'text-white');
  });

  registration.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-registration').classList.replace('text-white', 'active');

    if (order.classList.contains('show')) { toggleElement(order.id); }
    if (threeLevel.classList.contains('show')) { toggleElement(threeLevel.id); }
    if (foundation.classList.contains('show')) { toggleElement(foundation.id); }
  });

  // Ações Menu FOUNDATION
  foundation.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-foundation').classList.replace('active', 'text-white');
  });

  foundation.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-foundation').classList.replace('text-white', 'active');

    if (order.classList.contains("show")) { }

    if (order.classList.contains('show')) { toggleElement(order.id); }
    if (registration.classList.contains('show')) { toggleElement(registration.id); }
    if (threeLevel.classList.contains('show')) { toggleElement(threeLevel.id); }
  });

  // Ações Menu TRES NÍVEIS
  threeLevel.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-threelevel').classList.replace('active', 'text-white');

    if (threeLevel_1.classList.contains('show')) { toggleElement(threeLevel_1.id); }
    if (threeLevel_2.classList.contains('show')) { toggleElement(threeLevel_2.id); }
  });

  threeLevel.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-threelevel').classList.replace('text-white', 'active');

    if (order.classList.contains('show')) { toggleElement(order.id); }
    if (registration.classList.contains('show')) { toggleElement(registration.id); }
    if (foundation.classList.contains('show')) { toggleElement(foundation.id); }
  });

  // Ações Submenu NÍVEL 1
  threeLevel_1.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-threelevel1').classList.replace('active', 'text-white');
  });

  threeLevel_1.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-threelevel1').classList.replace('text-white', 'active');

    if (document.getElementById('threelevel2-collapse').classList.contains('show')) { toggleElement("threelevel2-collapse"); }
  });

  threeLevel_2.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-threelevel2').classList.replace('active', 'text-white');
  });

  threeLevel_2.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-threelevel2').classList.replace('text-white', 'active');

    if (threeLevel_1.classList.contains('show')) { toggleElement(threeLevel_1.id); }
  });

  // Filtros de pesquisa da tabela
  validateSearchFilter("v_table_filter")

  const sidebarToggle = document.body.querySelector('#sidebarToggle');
  if (sidebarToggle) {
    sidebarToggle.addEventListener('click', event => {
      event.preventDefault();
      document.body.classList.toggle('sidebar-hidden');
    });
  }
});

document.getElementById("v_table_filter").onchange = function () { validateSearchFilter(this); }

window.onresize = elementsOnResize;

function elementsOnResize() {
  var widthSize = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  var heightSize = window.innerHeight || document.documentElement.clientHeight | document.body.clientHeight;

  if (widthSize < 768) {
    document.body.classList.remove("sidebar-hidden")
  }
}

function validateSearchFilter(element) {
  let arrayFields = ["div_search_name", "div_search_cpf", "div_search_mail", "div_search_phone"];

  for (let i = 0; i < arrayFields.length; i++) {
    if (element.value != arrayFields[i].split("_")[2]) {
      hideElementById(arrayFields[i])
    }

    else { showElementById(arrayFields[i]) }
  }
}

function hideElementById(elementId) {
  let element = document.getElementById(elementId);
  element.style.display = "none";
}

function showElementById(elementId) {
  let element = document.getElementById(elementId);
  element.style.display = "";
}

function toggleElement(id) {
  new bootstrap.Collapse(`#${id}`, {
    toggle: true
  })
}

function listTable() {
  let arrayAccountant = [
    { "name": "Jean Miotto", "cpf": "087.238.529-92", "email": "jean.miotto@softwaresul.com.br", "phone": "(45) 99992 5057" },
    { "name": "Eduardo de Almeida Lima", "cpf": "012.345.678-90", "email": "eduardo.almeida@softwaresul.com.br", "phone": "(45) 99999 9999" },
    { "name": "José Andres Telles Pereira", "cpf": "012.345.678-90", "email": "jose.telles@softwaresul.com.br", "phone": "(45) 99999 9999" },
    { "name": "Vanderlei Junior Fornari", "cpf": "012.345.678-90", "email": "vanderlei.fornari@softwaresul.com.br", "phone": "(45) 99999 9999" }
  ]

  let tbody = document.getElementById("tbody");

  for (let i = 0; i < arrayAccountant.length; i++) {
    let tr = tbody.insertRow();
    let td_name = tr.insertCell();
    let td_cpf = tr.insertCell();
    let td_email = tr.insertCell();
    let td_phone = tr.insertCell();
    let td_action = tr.insertCell();

    td_name.innerText = arrayAccountant[i].name;
    td_cpf.innerText = arrayAccountant[i].cpf;
    td_email.innerText = arrayAccountant[i].email;
    td_phone.innerText = arrayAccountant[i].phone;
    td_action.innerHTML =
      '<div class="text-center">' +
      '<i class="bi bi-pencil-fill text-secondary px-1 icon_action" onclick="alert();"></i>' +
      '<i class="bi bi-trash-fill text-danger px-1 icon_action" onclick="alert();"></i>';
    '</div>'
  }
}