(() => {
  const forms = document.querySelectorAll('.needs-validation')

  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }

      form.classList.add('was-validated')
    }, false)
  })
})()

const money_symbol = "€";

document.addEventListener("DOMContentLoaded", function () {
  document.getElementById("money_symbol").innerHTML = money_symbol;

  var order = document.getElementById('order-collapse')
  var registration = document.getElementById('registration-collapse')
  var foundation = document.getElementById('foundation-collapse')
  var threeLevel = document.getElementById('threelevel-collapse')
  var threeLevel_1 = document.getElementById('threelevel1-collapse')
  var threeLevel_2 = document.getElementById('threelevel2-collapse')

  // Ações Menu ORDENS
  order.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-order').classList.replace('active', 'text-white');
  });

  order.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-order').classList.replace('text-white', 'active');

    if (threeLevel.classList.contains('show')) { toggleElement(threeLevel.id); }
    if (registration.classList.contains('show')) { toggleElement(registration.id); }
    if (foundation.classList.contains('show')) { toggleElement(foundation.id); }
  });

  // Ações Menu CADASTROS
  registration.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-registration').classList.replace('active', 'text-white');
  });

  registration.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-registration').classList.replace('text-white', 'active');

    if (order.classList.contains('show')) { toggleElement(order.id); }
    if (threeLevel.classList.contains('show')) { toggleElement(threeLevel.id); }
    if (foundation.classList.contains('show')) { toggleElement(foundation.id); }
  });

  // Ações Menu FOUNDATION
  foundation.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-foundation').classList.replace('active', 'text-white');
  });

  foundation.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-foundation').classList.replace('text-white', 'active');

    if (order.classList.contains("show")) {  }

    if (order.classList.contains('show')) { toggleElement(order.id); }
    if (registration.classList.contains('show')) { toggleElement(registration.id); }
    if (threeLevel.classList.contains('show')) { toggleElement(threeLevel.id); }
  });

  // Ações Menu TRES NÍVEIS
  threeLevel.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-threelevel').classList.replace('active', 'text-white');

    if (threeLevel_1.classList.contains('show')) { toggleElement(threeLevel_1.id); }
    if (threeLevel_2.classList.contains('show')) { toggleElement(threeLevel_2.id); }
  });

  threeLevel.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-threelevel').classList.replace('text-white', 'active');

    if (order.classList.contains('show')) { toggleElement(order.id); }
    if (registration.classList.contains('show')) { toggleElement(registration.id); }
    if (foundation.classList.contains('show')) { toggleElement(foundation.id); }
  });

  // Ações Submenu NÍVEL 1
  threeLevel_1.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-threelevel1').classList.replace('active', 'text-white');
  });

  threeLevel_1.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-threelevel1').classList.replace('text-white', 'active');

    if (document.getElementById('threelevel2-collapse').classList.contains('show')) { toggleElement("threelevel2-collapse"); }
  });

  threeLevel_2.addEventListener("hidden.bs.collapse", function () {
    document.getElementById('menu-threelevel2').classList.replace('active', 'text-white');
  });

  threeLevel_2.addEventListener("show.bs.collapse", function () {
    document.getElementById('menu-threelevel2').classList.replace('text-white', 'active');

    if (threeLevel_1.classList.contains('show')) { toggleElement(threeLevel_1.id); }
  });

  const sidebarToggle = document.body.querySelector('#sidebarToggle');
  if (sidebarToggle) {
    sidebarToggle.addEventListener('click', event => {
      event.preventDefault();
      document.body.classList.toggle('sidebar-hidden');
    });
  }

  const elements = document.querySelectorAll('.datepicker_input');
  for (const element of elements) {
    const datepicker = new Datepicker(element, {
      'format': 'dd/mm/yyyy',
      'autohide': true,
      'todayBtn': true,
      'todayBtnMode': 1,
      'todayHighlight': true,
      'language': 'pt-BR'
    });
  }
});

document.getElementById("v_decimal").onkeydown = function () { mascara(this, decimal, 3); }
document.getElementById("v_onlynumber").onkeydown = function () { mascara(this, onlyNumber); }
document.getElementById("v_cpf").onkeydown = function () { mascara(this, cpf); }
document.getElementById("v_cnpj").onkeydown = function () { mascara(this, cnpj); }
document.getElementById("v_cep").onkeydown = function () { mascara(this, cep); }
document.getElementById("v_number").onkeydown = function () { mascara(this, onlyNumber); }
document.getElementById("v_phone").onkeydown = function () { mascara(this, phone); }
document.getElementById("v_period_init").onkeydown = function () { mascara(this, toDate); }
document.getElementById("v_period_end").onkeydown = function () { mascara(this, toDate); }
document.getElementById("v_money").onkeydown = function () { mascara(this, money, [money_symbol, "money_symbol"]); }
document.getElementById("v_email").onblur = function () { mascara(this, email); }

window.onresize = elementsOnResize;

function elementsOnResize() {
  var widthSize = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  var heightSize = window.innerHeight || document.documentElement.clientHeight | document.body.clientHeight;

  if (widthSize < 768) {
    document.body.classList.remove("sidebar-hidden")
  }
}

function validateForm() {
  document.getElementById('modal_validateForm_message').innerHTML = 'O e-mail informado é inválido. Por favor, verifique.'
  showModal();
}

function showModal() {
  var modal_validateForm = new bootstrap.Modal(document.getElementById("modal_validateForm"), { keyboard: false });
  modal_validateForm.show();
}

function toggleElement(id) {
  new bootstrap.Collapse(`#${id}`, {
    toggle: true
  })
}